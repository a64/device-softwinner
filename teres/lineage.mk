PRODUCT_RELEASE_NAME := teres

$(call inherit-product, device/softwinner/teres_pinebook_common/common.mk)
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 1647483648
KERNEL_CFG_NAME := olimex_teres1
WITHOUT_CHECK_API := true

PRODUCT_BRAND := Allwinner
PRODUCT_NAME := lineage_teres
PRODUCT_DEVICE := tulip-chiphd
PRODUCT_MODEL := Teres
PRODUCT_MANUFACTURER := Allwinner
TARGET_RELEASETOOL_OTA_FROM_TARGET_SCRIPT = "device/softwinner/tulip-chiphd/pinebook/no_ota.sh"
