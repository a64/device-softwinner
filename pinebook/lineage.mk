PRODUCT_RELEASE_NAME := pinebook

$(call inherit-product, device/softwinner/teres_pinebook_common/common.mk)
WITHOUT_CHECK_API := true

PRODUCT_BRAND := Allwinner
PRODUCT_NAME := lineage_pinebook
PRODUCT_DEVICE := tulip-chiphd
PRODUCT_MODEL := Pinebook
PRODUCT_MANUFACTURER := Allwinner
TARGET_RELEASETOOL_OTA_FROM_TARGET_SCRIPT = "device/softwinner/tulip-chiphd/pinebook/no_ota.sh"
